---
layout: home
title: Kia ora, I'm Benet.
postsHeading: Latest posts
archiveButtonText: See all posts
metaDesc: 'Welcome to my little corner of the galaxy. Take a look around and if you like what you see be sure to connect.'
socialImage: ''
---

Welcome to my little corner of the galaxy. Take a look around and if you like what you see be sure to connect.
