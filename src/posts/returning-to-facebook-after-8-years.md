---
title: Returning to Facebook after 8 years
date: 2021-02-07
tags:
  - Facebook
  - Social media
  - Community
---

After 8 years of not using my real name on Facebook or having a proper account I have had to compromise and rejoin. To say that it "fucking sucks" would be a complete understatement. Over the past couple of years I have missed out on various opportunities and I feel quite disconnected from my community. I'm sure this isn't solely related to not being on FB but not being on there hasn't helped.

In the first half of 2013 my wife and I were moving back from Australia to New Zealand to start a family. I had been contemplating leaving the ugly blue giant for years due to privacy concerns and its overall negative effects on society. I thought that moving countries would be a great time to leave and it was.

The switch was ok; I spent time nurturing my own social network whether that was swapping phone numbers, email addresses or encouraging people to use other platforms like Signal, Mastodon, Telegram and Matrix. I even succeeded in getting my family on Signal, that was a huge win. When our first child came along I set up a private website for [whānau](https://en.wikipedia.org/wiki/Wh%C4%81nau) and friends to view baby photos.

But recently there have been a few occasions where not being on Facebook has been a huge challenge.

Occasion number 1: My eldest daughter has just started her second year of school. The parents of her peer group all use FB for general socialising and organising things for the kids. Some of the parents have my mobile number and my wife is on FB and is included in some of the group chats but I am the stay-at-home parent and not being in that primary loop means some things slip. Last week we almost missed out on a friends birthday party because details from group chats including my wife weren't passed on and they were sprung on me at the last minute.

Occasion number 2: Before kids I used to DJ a lot on the radio and at gigs around the city. I have kept up the radio and streaming side of things but my live game has been in hibernation. So last year I upgraded my kit and tried to get more gigs. I missed out on a couple due to mixed messages between platforms and me using pseudonyms.

In both of these situations the slight lack of communication and inclusion is not personal; it's actually how Facebook is engineered. FB is constantly fighting for your attention, that is one of the ways it makes money. They have and keep your attention and sell that to advertisers. When you spend so much time in the blue bubble the other things in your life get less attention and when you're a busy parent or anyone else living a busy life things like that just happen.

Missing out on these opportunities and not being present in the minds of some of my friends and community members has really been affecting my mental health. Rejoining Facebook won't necessarily fix all my problems and will no doubt create new ones but I'll see how I go. There seems to be a bit of a momentum to move away from Facebook but it's not at tipping point yet.

If you come across someone that doesn't use some kind of tech for privacy reasons try to spare a little extra attention to include them in your life; and maybe even ask them what the best way is to keep in touch with them, you will make their day 😊

What is your experience with Facebook and social media? Have you ever wanted to leave? Have you left and had to return? Let me know your thoughts on [Mastondon](https://octodon.social/@drh) or [Twitter](https://twitter.com/drhitchcocknz).
