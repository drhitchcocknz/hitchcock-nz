---
title: My tidy little header
date: '2020-11-22'
tags:
  - HTML
  - CSS
  - IndieWeb
---

What do you think of my tidy little header 😅

I've been working on making this website more [IndieWeb](https://indieweb.org/) friendly. [Andy Bell](https://twitter.com/piccalilli_) and his friends who built this 11ty based template had built some IndieWebNess into it already and I am just standing on their shoulders. I also changed out the colours from the original template and I am pretty happy with how it looks in light mode although dark mode needs a little love.

I had some great success with building the header. I had already built most of it on a previous version of this site so it was just a matter of transferring the code and using CSS Grid and Flexbox to make it work with the theme. I am primarily a Stay-At-Home-Dad (SAHD) these days so any chance I find to dabble with some HTML and CSS I jump at.
