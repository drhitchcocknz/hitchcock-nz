---
title: '👍 "CSS :not selector"'
date: '2020-12-07'
tags:
  - Likes
  - Webmention
---

<div class="h-entry">
<div class="h-cite u-like-of">
  Liked <a class="u-url" href="https://daily-dev-tips.com/posts/css-not-selector/">a post</a> by
  <span class="p-author h-card">
    <a class="u-url p-name" href="https://daily-dev-tips.com/">Chris Bongers.</a>
  </span>:
  <blockquote class="e-content">
    <p>CSS :not selector</p>
  </blockquote>
</div>
</div>
