---
title: McIver's North
date: 2022-02-13
tags:
  - Melbourne
  - Coffee
  - Brunswick West
---

2022... Global Pandemic... 2 young children... Why not move countries ?🤗

So at the end of January we moved from Ōpawaho, Ōtautahi, Aotearoa | Woolston, Christchurch, New Zealand to Naarm | Brunswick West, Melbourne. What a way to start the year! Boom!

![McIvers North, Brunswick West.](/images/IMG_1450.jpg 'McIvers North, Brunswick West.')
