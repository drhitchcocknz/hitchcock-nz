---
title: Compassion 2020
date: '2020-11-08'
tags:
  - politics
  - world peace
---

I just woke up to the news that Joe Biden is projected to be the next president of the United States of America 🥳. Even though I live in New Zealand the lead up to the US elections has been quite stressful. Donald Trump and the Republican Party that he has lead have done many things that I disagree with and in my opinion humans on planet Earth are more divided as a result of his presidency. But Mr Biden is not going to single handedly bring us all together. This is something we all need to work on.

Recently someone I respect and admire in the web development industry announced that they had voted for Trump and the Republicans; not because they liked Trump but because they believe that the policies of that party better aligned with their religious beliefs. The #TwitterStorm was swift and brutal. As usual there was a mix of comments, from supportive and constructive to down right nasty, along with plenty of bafflement. Now I don't agree with how this person voted, nor do I see how this person's religious beliefs align with Trump's presidency but we need to stop yelling at each other. We need to listen, actually listen. I've learnt this the hard way.

Much to my surprise a couple of my family members are either Trump friendly or openly pro-Trump. When I first found this out it completely threw me. These family members are loving, family focused people and active in their religious community. One night I got into argument with one of them about it and the relationship has not be great since.

We need to truly listen to others even if you don't agree with them. No one likes being lectured. If you get into a discussion with someone who has different views take a moment and be mindful of your response, figure out where the common ground is. Ask questions about their viewpoint, kindly ask them to explain things that don't make sense to you. And in the end remember it is very likely that nothing you say will change their mind.

If we want the world to be a warmer, friendlier, happier and more peaceful place we need to listen ✌️.
