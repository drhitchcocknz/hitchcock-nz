---
title: Working through setting up Webmentions.
date: '2021-01-02'
tags:
  - IndieWeb
  - Webmentions
---

Happy new year! May 2021 bring you some joy 😬

I'm in the process of working through a tutorial on setting up [Webmentions](https://indieweb.org/Webmention) on this 11ty based website. As far as I can tell the code is set up correctly but there seems to be an issue connecting to webmentions.io or it isn't writing to the cache properly? The dashboard in websockets.io can see that likes have been posted in Twitter. Do you think you might have time and patience to help? I'm willing to help you with something in return. 🙏

Here's where I am at... I started with the Hylia theme from Andy Bell and I am following the Webmentions tutorial from Sia Karamalegos.
<https://hylia.website/>
<https://sia.codes/posts/webmentions-eleventy-in-depth/>

The page that has been liked on Twitter is:
<https://www.hitchcock.nz/posts/how-do-people-focus-on-what-theyre-doing-with-their-dev-life/>

The link to my repo is:
<https://gitlab.com/starlifter-nz/hitchcock-nz>

The links to the relevant code are:

- Data
  <https://gitlab.com/starlifter-nz/hitchcock-nz/-/blob/master/src/_data/metadata.json>
- JS
  <https://gitlab.com/starlifter-nz/hitchcock-nz/-/blob/master/.eleventy.js>
  <https://gitlab.com/starlifter-nz/hitchcock-nz/-/blob/master/_11ty/filters.js>
  <https://gitlab.com/starlifter-nz/hitchcock-nz/-/blob/master/src/_data/webmentions.js>
- Templates
  <https://gitlab.com/starlifter-nz/hitchcock-nz/-/blob/master/src/_includes/layouts/base.njk>
  <https://gitlab.com/starlifter-nz/hitchcock-nz/-/blob/master/src/_includes/webmentions.njk>
  <https://gitlab.com/starlifter-nz/hitchcock-nz/-/blob/master/src/_includes/webmention.njk>
  <https://gitlab.com/starlifter-nz/hitchcock-nz/-/blob/master/src/_includes/layouts/post.njk>

![Screenshot from www.webmentions.io as described in caption.](/images/Webmentions_screenshot.jpg 'This screenshot from Webmentions.io shows that the data is there waiting to be pulled into my site.')

My cache currently looks like this:

```
{
  "lastFetched": "2021-01-01T06:52:31.883Z",
  "children": []
}
```

If there is anything else just holla! Thanks so much to the #IndieWeb community for the help so far.
