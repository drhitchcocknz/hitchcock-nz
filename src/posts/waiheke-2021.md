---
title: Waiheke 2021
date: 2021-04-20
tags:
  - Travel
  - Holiday
---

So for the first time in a very long time I am on a proper holiday. Not to see family, not to do anything or get anything done in particular but just to be. It is also the first time ever that I am away from my two young daughters and my wife. I am the primary stay-at-home-parent and have been on-and-off for the past 5 years. This is just such a lovely treat.
