---
title: '👍 "Why are you still posting your content to social media instead of your own website?"'
date: '2020-12-27'
tags:
  - Likes
  - Webmention
---

<div class="h-entry">
<div class="h-cite u-like-of">
  Liked <a class="u-url" href="https://boffosocko.com/2020/12/28/55784386/">a post</a> by
  <span class="p-author h-card">
    <a class="u-url p-name" href="https://boffosocko.com/">Chris Aldrich</a>
  </span>:
  <blockquote class="e-content">
    <p>Why are you still posting your content to social media instead of your own website</p>
  </blockquote>
</div>
</div>
