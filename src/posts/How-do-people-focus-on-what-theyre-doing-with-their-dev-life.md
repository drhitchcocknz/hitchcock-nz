---
title: 'How do people focus on what they’re doing with their (dev) life?'
date: '2020-12-29'
tags:
  - Productivity
  - Stay-at-home-Dad
  - SAHD
  - Web Dev
---

I know 2020 has been a super weird thing for everyone. This year was always gonna be strange for me as I was moving from being a freelance web developer to a full-time stay-at-home-Dad (SAHD). Last time when I was a SAHD for our eldest child my dev game dropped quite significantly and I found it quite hard to get my head back into that space when I returned. WebDev also moves frighteningly fast which adds to the challenges. This time around I have definitely put more time and energy into keeping my eye on the industry; what it is doing and where it's going. Beyond listening to podcasts and reading articles I am not sure what to do next.

One thing that I am more confident in this time around is that I know my web dev strengths are in HTML and CSS. This can be a hard skill to sell oneself, especially in a small job market like [Otāutahi](https://en.wikipedia.org/wiki/Christchurch) where I am based. I think it is smarter to work on that than try to become a Javascript expert.

I just can't figure what I need to do next. I am having trouble getting focused. Does anyone have any simple tips or tricks? The simpler, the better.
