// _11ty/filters.js

const {DateTime} = require('luxon');

module.exports = {
  readableDateFromISO: (dateStr, formatStr = "dd LLL yyyy 'at' hh:mma") => {
    return DateTime.fromISO(dateStr).toFormat(formatStr);
  }
};
